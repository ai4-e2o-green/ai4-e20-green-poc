# AI4Copernicus - demo

Simple web map that shows timeseries of change detection rasters.


## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. 
