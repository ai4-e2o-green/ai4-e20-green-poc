const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
var webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const { env } = require('process');

const PATHS = {
    src: path.resolve(__dirname, 'src'),
    img: path.resolve(__dirname, 'src/assets/images'),
    build: path.resolve(__dirname, 'dist')
}

const WEBSITE_BASE_URL = {
    production: JSON.stringify('/'),
    development: JSON.stringify('/dist/')
}

module.exports = (env, argv) => {

    return {
        context: PATHS.src,
        entry: {
            main: './js/index.js',
            mowing: './js/index.js',
            workspace3: './js/index.js',
            workspace4: './js/index.js'
        },
        output: {
            path: PATHS.build,
            filename: '[name].js',
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                }
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: 'index.html',
                chunks: ['main'],
                layersJSON: JSON.stringify('./resources/watering_layers.json')
            }),
            new HtmlWebpackPlugin({
                template: 'mowing.html',
                filename: 'mowing.html',
                chunks: ['mowing'],
                layersJSON: JSON.stringify('./resources/mowing_layers.json')
            }),
            new HtmlWebpackPlugin({
                template: 'workspace3.html',
                filename: 'workspace3.html',
                chunks: ['workspace3'],
                layersJSON: JSON.stringify('./resources/workspace3_layers.json')
            }),
            new HtmlWebpackPlugin({
                template: 'workspace4.html',
                filename: 'workspace4.html',
                chunks: ['workspace4'],
                layersJSON: JSON.stringify('./resources/workspace4_layers.json')
            }),
            new CopyWebpackPlugin({
                patterns: [
                        {from: "assets", to: "assets"},
                        {from: "styles", to: "styles"},
                        {from: "resources", to: "resources"},
                    ]
                }
            ),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            }),
            new webpack.DefinePlugin({
                'WEBSITE_BASE_URL': WEBSITE_BASE_URL[argv.mode]
            })
        ]
    }
}
