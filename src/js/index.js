import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min';
import 'ol/ol.css';

import {Control, defaults as defaultControls} from 'ol/control';
import {Map, View} from 'ol';
import {fromLonLat, transformExtent} from 'ol/proj';

import OSM from 'ol/source/OSM';
import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ.js';

document.addEventListener('DOMContentLoaded', function() {
    var sidenavOptions = {
        edge: 'left',
        preventScrolling: false
    }
    M.Sidenav.init(document.querySelectorAll('.sidenav'), sidenavOptions);

    document.querySelector('#nextButton').addEventListener('click', nextLayer, false);
    document.querySelector('#prevButton').addEventListener('click', previousLayer, false);
});

let map;
let rasterLayer;
let currentLayerDate;

let layers_fpath;
let colormap;

if (window.PAGE === 'index') {
  layers_fpath = './resources/layers_watering.json';
  colormap = './resources/colormap_watering.png'
} else if (window.PAGE === 'mowing') {
  layers_fpath = './resources/layers_mowing.json'
  colormap = './resources/colormap_mowing.png'
} else if (window.PAGE === 'workspace3') {
  layers_fpath = './resources/layers_workspace3.json'
  colormap = './resources/colormap_workspace3.png'
} else if (window.PAGE === 'workspace4') {
  layers_fpath = './resources/layers_workspace4.json'
  colormap = './resources/colormap_workspace4.png'
}

console.log(layers_fpath);


class LayerSwitcherControl extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const layerTree = document.createElement('div');
    layerTree.className = 'layer-switch-menu right';

    const currentRaster = '<p><span id="raster-name"></span></p><br>';


    const arrow_left = '<button class="btn waves-effect waves-light blue-grey darken-3" id="prevButton" name="action">' +
                       'Previous<i class="material-icons left">arrow_back</i></button>';
    const arrow_right = '<button class="btn waves-effect waves-light blue-grey darken-3" id="nextButton" name="action">' +
                       'Next<i class="material-icons left">arrow_forward</i></button>';

    layerTree.innerHTML = currentRaster + arrow_left + '&nbsp;' + arrow_right;

    super({
      element:layerTree,
      target: options.target,
    });
  }
}

class MapLegendControl extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const mapLegend = document.createElement('div');
    mapLegend.className = 'map-legend right';
    mapLegend.innerHTML = '<img src="' + colormap + '" alt="map legend">';

    super({
      element:mapLegend,
      target: options.target,
    });

  }
}

const osmLayer = new TileLayer({
  source: new OSM()
});

const mapExtent = transformExtent([11, 42, 20.5, 47.2], 'EPSG:4326', 'EPSG:3857');

map = new Map({
    target: 'map',
    layers: [osmLayer],
    controls: defaultControls().extend([
      new LayerSwitcherControl(),
      new MapLegendControl()
    ]),
    view: new View({
        center: fromLonLat([16.02026, 45.8285]),
        zoom: 15,
        extent: mapExtent
    })
});

// load layers from json
loadLayerFromJson();


function loadLayerFromJson(action=undefined) {
  fetch(layers_fpath)
    .then((response) => response.json())
    .then((json) => {

      let layerObject;
  
      if (currentLayerDate !== undefined) {
        console.log(currentLayerDate);

        if (action == 'previous') {
          layerObject = getPreviousObject(currentLayerDate, json);
        }
        else {
          layerObject = getNextObject(currentLayerDate, json);
        }

      } else {
        // get first layer in json
        layerObject = json[0];
      }

      // add layer to map
      addTiledLayerToMap(layerObject.tiles_url);

      // set value for current layer date object and html text
      currentLayerDate = layerObject.date;
      document.getElementById('raster-name').innerText = layerObject.date;

    });
}


function addTiledLayerToMap(tilesURL, clearOtherLayers=true) {
  
  if (clearOtherLayers == true) {
    // clear existing layer
    map.removeLayer(rasterLayer);
  }

  rasterLayer = new TileLayer({
    source: new XYZ({
      url: tilesURL + '/{z}/{x}/{y}.png'
    })
  });

  map.addLayer(rasterLayer);
}


function previousLayer() {
  loadLayerFromJson('previous');
}

function nextLayer() {
  loadLayerFromJson('next');
}


function getPreviousObject(date, array) {
  let found = false;
  for (let i = array.length - 1; i >= 0; i--) {
    if (found) {
      return array[i];
    }
    if (array[i].date === date) {
      found = true;
    }
  }

  M.toast({html: 'There are no older rasters to show!', displayLength: 2000});
  return null;
}

function getNextObject(date, array) {
  let found = false;
  for (let i = 0; i < array.length; i++) {
    if (found) {
      return array[i];
    }
    if (array[i].date === date) {
      found = true;
    }
  }
  M.toast({html: 'There are no newer rasters to show!', displayLength: 2000});
  return null;
}
